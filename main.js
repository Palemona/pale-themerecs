$(document).ready(function () {
    //wow animations
    new WOW().init();

    $("#search_button").click(function () {
        $(".input_search").toggleClass("search-active");
    });

    /*
    if(window.location.href.indexOf("/post/") === -1) {
        $(".like, .reblog, .perma").hide();
    }*/

    setInterval(function () {
        var today = new Date();
        var extra_min = today.getMinutes() < 10 ? '0' : '';
        var extra_hrs = today.getHours() < 10 ? '0' : '';
        var date = today.toLocaleString('en-US', {
            month: 'long',
        }) + ' ' + today.getDate() + ' ' + today.getFullYear();
        var time = extra_hrs + today.getHours() + ":" + extra_min + today.getMinutes();
        var dateTime = date + '  - ' + time;

        $(".current_date").html(dateTime);
    }, 1000);

    $(".logs_dl").html(`<dt>06/21/2023</dt>
                        <dd>Released 'Do-Re-Mi' theme</dd>
                        <dt>05/12/2023</dt>
                        <dd>Released 'POKÉDEX 98' page</dd>
                        <dt>01/17/2023</dt>
                        <dd>Released 'See The Stars With Me, Again' theme</dd>
                        <dt>01/16/2023</dt>
                        <dd>Hide loading screen option on themes</dd>
                        <dt>08/28/2022</dt>
                        <dd>Released 'Glazed' theme</dd>
                        <dt>07/11/2022</dt>
                        <dd>Released 'Heart of Cards' Page!</dd>
                        <dt>06/24/2022</dt>
                        <dd>New layout for site!</dd>
                        <dt>04/15/2022</dt>
                        <dd>Doodling 3x1 on Theme Garden</dd>
                        <dt>03/10/2022</dt>
                        <dd>Magical Doremi icons fix</dd>
                    `);

    var stack = ["#welcome", "#recent"];
    $(".shortcut").click(function () {
        let targets = ['#tos', '#recent', '#logs', '#welcome'];
        let target = $(this).data("target");
        $(target).show();
        $(target).addClass("animate__zoomIn");
        $(target).removeClass("animate__zoomOut");
        $("#mobile_backdrop").show();
        $(target).css({ "visibility": "visible" });
        if (!stack.find(element => element === target)) {
            stack.push(target);
        } else {
            stack.push(...stack.splice(stack.findIndex(v => v === target), 1));
        }
        console.log("stack", stack);
        let rest_targets = targets.filter(id => id != target);

        var zindex = 53;
        for (let index = stack.length - 1; index >= 0; index--) {
            $(stack[index]).css("z-index", zindex);
            zindex--;
        }
    });

    $(function () {
        $(".draggable").draggable({
            stack: ".side_box"
        });
    });

    /*
    $(".side_box").click(function () {
       let target = '#'+$(this).attr('id');
       if(!stack.find(element => element === target)){
            stack.push(target);
        } else {
            stack.push(...stack.splice(stack.findIndex(v => v === target), 1));
        }
       var zindex = 53;
       for(let index = stack.length - 1; index >= 0; index--) {
            $(stack[index]).css("z-index", zindex);
            zindex--;
        }
    });*/

    
    $(".sidebox_close").click(function () {
        let target = $(this).data("target");
        $(target).addClass("animate__zoomOut");
        $(target).removeClass("animate__zoomIn");
        $("#mobile_backdrop").hide();
        setTimeout(function () {
            $(target).css({ "visibility": "hidden" });
        }, 500);

    });
    

    if (window.innerWidth < 769) {
        if ($(window).scrollTop() > 100) {
            $(".menu_mobile").addClass("nav_active");
        } else {
            $(".menu_mobile").removeClass("nav_active");
        }
    } else {
        $(window).on("scroll", function () {
            if ($(window).scrollTop() > 300) {
                $(".sidenav").addClass("nav_active");
                $(".bg_flowers").addClass("bg_flowers_active");
            } else {
                $(".sidenav").removeClass("nav_active");
                $(".bg_flowers").removeClass("bg_flowers_active");
            }
        });
    }
    $(window).on("scroll", function () {
        scrollFunction();
    });

    //dark mode 
    var theme = localStorage.getItem('theme') ? localStorage.getItem('theme') : "light";
    document.documentElement.setAttribute('data-theme', theme);
    if (theme === 'dark') {
        $(".btn_dark_mode").html('<i class="bi bi-sun"></i>');
    } else {
        $(".btn_dark_mode").html('<i class="bi bi-moon-stars"></i>');
    }

    $('.btn_dark_mode').click(function () {
        if (theme === 'dark') {
            $(".btn_dark_mode").html('<i class="bi bi-moon-stars"></i>');
            localStorage.setItem('theme', 'light');
            document.documentElement.setAttribute('data-theme', 'light');
            theme = "light";
        } else {
            $(".btn_dark_mode").html('<i class="bi bi-sun"></i>');
            localStorage.setItem('theme', 'dark');
            document.documentElement.setAttribute('data-theme', 'dark');
            theme = "dark";
        }
    });

    if ($('.noteslink').is(':empty')) {
        $(".fecha").hide();
    };

    $('.controls').click(function () {
        $(".iframe-controls--desktop").toggle("slow")
    });

    $('.follow').click(function () {
        window.location.href = "https://www.tumblr.com/follow/themesbypale";
    });

    $('.dashboard').click(function () {
        window.location.href = "https://www.tumblr.com/dashboard";
    });



    var showMenu = false;
    $('#menu_open, #closemenu').click(function () {
        showMenu = !showMenu;
        if (showMenu) {
            $("#sidemenu").css("transform", "translateX(0px)");
            $("html").css("overflow", "hidden");
        } else {
            $("#sidemenu").css("transform", "translateX(-100vw)");
            $("html").css("overflow", "auto");
        }
    });
});

function goTo(uri) {
    window.location.href = uri;
}

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("top_button").style.display = "flex";
    } else {
        document.getElementById("top_button").style.display = "none";
    }

}
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

function closeIFrame() {
    var f = document.getElementById('ditto');
    f.src = f.src;
}
